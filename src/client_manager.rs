use crate::config::Config;
use crate::ssl_manager::get_host_pair;
use nomhttp::http_chunk::HttpChunk;
use nomhttp::http_header::HttpHeader;
use nomhttp::http_method::HttpMethod;
use nomhttp::http_request::HttpRequest;
use nomhttp::http_response::HttpResponse;
use nomhttp::http_response_status::HttpResponseStatus;
use nomhttp::http_transfer_encoding::HttpTransferEncoding;
use nomhttp::http_version::HttpVersion;

use core::pin::Pin;
use log::{debug, error, info};
use nom::bytes::streaming::{tag, take, take_until, take_while};
use nom::character::is_alphanumeric;
use nom::combinator::{iterator, map_res};
use nom::error::Error as NError;
use nom::sequence::terminated;
use nom::IResult;
use nom_bufreader::bufreader::BufReader;
use nom_bufreader::Parse;
use openssl::{
    pkey::{PKey, Private},
    ssl::{HandshakeError, Ssl, SslAcceptor, SslConnector, SslMethod, SslStream, SslVerifyMode},
    x509::X509,
};
use rusqlite::Connection;
use std::{
    error::Error,
    io::{Read, Write},
    net::{TcpStream, ToSocketAddrs, IpAddr, Ipv4Addr},
    str::FromStr,
    time::Instant,
};

pub fn client_manager(s: TcpStream, ca_cert: X509, ca_key_pair: PKey<Private>, config: Config) {
    if config.force_target().is_empty() {
        let mut reader = BufReader::with_capacity(1024 * 1024, s);
        let req = handle_request(&mut reader);
        let req = match req {
            None => return,
            Some(x) => {
                let (method, path, version, headers, body, raw) = x;
                HttpRequest::new(method, path, version, headers, body, raw, false)
            }
        };
        debug!("{:?}", req);

        /* prevent attacking 127.0.0.1 */
        if !config.allow_localhost() {
            let mut mask = req.destination().unwrap().to_socket_addrs().unwrap();

            if req.path().contains("127.0.0.1") || mask.any(|x| x.ip() == IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1))) {
                return;
            }
        }

        match req.method() {
            HttpMethod::Connect => {
                /* https */
                debug!("New HTTPS request !");
                if let Err(e) = handle_https_request(reader, req, ca_cert, ca_key_pair, config) {
                    error!("Error while reading HTTPS request: {:?}", e)
                }
            }
            _ => {
                /* http */
                debug!("New HTTP request !");
                if let Err(e) = handle_cleartext_request(reader, req, config) {
                    error!("Error while reading HTTP request: {:?}", e)
                }
            }
        }
    } else {
        let now = Instant::now();
        let remote_addr = format!("{}", s.peer_addr().unwrap());

        let host_no_port = if config.force_target().contains(':') {
            config.force_target().split(':').take(1).collect()
        } else {
            config.force_target().to_string()
        };


        let mut buff: [u8; 10] = [0; 10];

        s.peek(&mut buff).unwrap();

        if buff[..10] == [22, 3, 1, 2, 0, 1, 0, 1, 252, 3] || buff[..10] == [22, 3, 1, 0, 244, 1, 0, 0, 240, 3] {
            /* SSL handshake */
            debug!("This is HTTPs request");
            let conn = Connection::open(format!("{}/hist.db", config.directory())).unwrap();
            let (cert, pair) = get_host_pair(&conn, &host_no_port, &ca_cert, &ca_key_pair);
            let mut acceptor = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
            acceptor.set_private_key(&pair).unwrap();
            acceptor.set_certificate(&cert).unwrap();
            acceptor.add_extra_chain_cert(ca_cert).unwrap();
            acceptor.set_verify(SslVerifyMode::NONE);
            acceptor.check_private_key().unwrap();
            let acceptor = acceptor.build();
            let ssl = Ssl::new(acceptor.context()).unwrap();
            let mut sslstream = SslStream::new(ssl, &s).unwrap();
            debug!("Accepting ssl stream");
            Pin::new(&mut sslstream).accept().unwrap();
            debug!("SSL Handshake done");
            let mut reader = BufReader::with_capacity(1024 * 1024, sslstream);
            let req = handle_request(&mut reader);
            let req = match req {
                None => return,
                Some(x) => {
                    let (method, path, version, headers, body, raw) = x;
                    HttpRequest::new(method, path, version, headers, body, raw, false)
                }
            };
            debug!("Received request from client via SSLStream");
            let mut rstream_std = TcpStream::connect(config.force_target()).unwrap();
            let resp = match config.force_https() {
                true => {
                    let mut rstream = ssl_connect(&rstream_std, &host_no_port).unwrap();
                    rstream.write_all(&req.as_bytes()).unwrap();
                    debug!("SSL handshake to destination is done");
                    let rstreamreader = BufReader::with_capacity(1024 * 1024, rstream);
                    handle_response(rstreamreader, reader.into_inner()).unwrap()
                },
                false => {
                    rstream_std.write_all(&req.as_bytes()).unwrap();
                    let rstreamreader = BufReader::with_capacity(1024 * 1024, rstream_std);
                    handle_response(rstreamreader, reader.into_inner()).unwrap()
                }
            };

            info!(
                "{} {} [{}] - {:?}",
                req.method(),
                req.path(),
                resp.status(),
                now.elapsed()
            );
            insert_complete_request(&req, &resp, &remote_addr, config.force_https(), &config, &now.elapsed());
        } else {
            debug!("This is standard HTTP request");
            let mut reader = BufReader::with_capacity(1024 * 1024, s);
            let req = handle_request(&mut reader);
            let req = match req {
                None => return,
                Some(x) => {
                    let (method, path, version, headers, body, raw) = x;
                    HttpRequest::new(method, path, version, headers, body, raw, false)
                }
            };
            let s = reader.into_inner();
            debug!("{}", req);
            let mut rstream_std = TcpStream::connect(config.force_target()).unwrap();
            debug!("Connected to destination");
            let resp = match config.force_https() {
                true => {
                    let mut rstream = ssl_connect(&rstream_std, &host_no_port).unwrap();
                    rstream.write_all(&req.as_bytes()).unwrap();
                    debug!("SSL handshake to destination is done");
                    let rstreamreader = BufReader::with_capacity(1024 * 1024, rstream);
                    handle_response(rstreamreader, s).unwrap()
                },
                false => {
                    rstream_std.write_all(&req.as_bytes()).unwrap();
                    let rstreamreader = BufReader::with_capacity(1024 * 1024, rstream_std);
                    handle_response(rstreamreader, s).unwrap()
                },
            };
            info!(
                "{} {} [{}] - {:?}",
                req.method(),
                req.path(),
                resp.status(),
                now.elapsed()
            );
            insert_complete_request(&req, &resp, &remote_addr, config.force_https(), &config, &now.elapsed());
        }
    }

}

fn parse_method(i: &[u8]) -> IResult<&[u8], HttpMethod, ()> {
    let (i, method_au8) = take_while(is_alphanumeric)(i)?;
    let (_, method_s) = map_res(take(method_au8.len()), std::str::from_utf8)(method_au8)?;
    let method = HttpMethod::from_str(method_s).unwrap();

    let (i, _) = take(1u8)(i)?; // take space

    Ok((i, method))
}

fn parse_path(i: &[u8]) -> IResult<&[u8], String, ()> {
    let (i, path_au8) = take_until(" ")(i)?;

    let path = path_au8.iter().map(|&b| b as char).collect();

    let (i, _) = take(1u8)(i)?; // take space

    Ok((i, path))
}

fn parse_version(i: &[u8]) -> IResult<&[u8], HttpVersion, ()> {
    let (i, version_au8) = take_until("\r\n")(i)?;
    let (_, version_s) = map_res(take(version_au8.len()), std::str::from_utf8)(version_au8)?;
    let version = HttpVersion::from_str(version_s).unwrap();

    let (i, _) = take(2u8)(i)?; // take CRLF after Http version

    Ok((i, version))
}

fn parse_header(i: &[u8]) -> IResult<&[u8], HttpHeader, ()> {
    let (value_au8, name_au8) = take_until(": ")(i)?;
    let name = name_au8.iter().map(|&b| b as char).collect();
    let (value_au8, _) = take(2u8)(value_au8)?;
    let value: String = value_au8.iter().map(|&b| b as char).collect();

    let header = HttpHeader::new(name, value);

    Ok((value_au8, header))
}

fn parse_headers(i: &[u8]) -> IResult<&[u8], Vec<HttpHeader>, ()> {
    let (rest, headers_au8) = terminated(take_until("\r\n\r\n"), tag("\r\n"))(i)?;
    let headers_au8 = [headers_au8, rest].concat();

    let mut headers_iter = iterator(
        &headers_au8[..],
        terminated(take_until("\r\n"), tag::<_, _, NError<_>>("\r\n")),
    );

    let headers: Vec<HttpHeader> = headers_iter
        .map(parse_header)
        .filter_map(|r| r.ok())
        .map(|v| v.1)
        .collect();

    Ok((&rest[2..], headers)) // avoid last \r\n
}

type ParseRequestRetType = (
    HttpMethod,
    String,
    HttpVersion,
    Vec<HttpHeader>,
    Vec<u8>,
    Vec<u8>,
);

fn nom_parse_request(i: &[u8]) -> IResult<&[u8], ParseRequestRetType, ()> {
    let raw = i.to_vec();

    let (i, method) = parse_method(i)?;
    let (i, path) = parse_path(i)?;
    let (i, version) = parse_version(i)?;
    let (body, headers) = parse_headers(i)?;

    let head_len = raw.len() - body.len();
    let r = (
        method,
        path,
        version,
        headers,
        body.to_vec(),
        raw[..head_len].to_vec(),
    );
    Ok((body, r))
}

pub fn handle_request<T>(reader: &mut BufReader<T>) -> Option<ParseRequestRetType>
where
    T: Read + Write,
{
    let r = reader.parse(nom_parse_request).ok()?;
    Some(r)
}

fn parse_status_version(i: &[u8]) -> IResult<&[u8], HttpVersion, ()> {
    debug!("Entering parse_status_version: {:?}", i);
    let (i, version_au8) = take_until(" ")(i)?;
    let (_, version_s) = map_res(take(version_au8.len()), std::str::from_utf8)(version_au8)?;
    let version = HttpVersion::from_str(version_s).unwrap();

    let (i, _) = take(1u8)(i)?; // take the ' '
    Ok((i, version))
}

fn parse_status_code(i: &[u8]) -> IResult<&[u8], usize, ()> {
    debug!("Entering parse_status_code: {:?}", i);
    let (i, code_au8) = take_until(" ")(i)?;
    let (_, code_s) = map_res(take(code_au8.len()), std::str::from_utf8)(code_au8)?;
    let code = code_s.parse::<usize>().unwrap();

    let (i, _) = take(1u8)(i)?; // take the ' '
    Ok((i, code))
}

fn parse_status_text(i: &[u8]) -> IResult<&[u8], Vec<u8>, ()> {
    debug!("Entering parse_status_text: {:?}", i);
    let (i, msg_au8) = take_until("\r\n")(i)?;
    let msg: Vec<u8> = msg_au8.to_vec();

    let (i, _) = take(2u8)(i)?; // take the '\r\n'

    Ok((i, msg))
}

fn parse_status_line(i: &[u8]) -> IResult<&[u8], HttpResponseStatus, ()> {
    debug!("Entering parse_status_line: {:?}", i);
    let (i, version) = parse_status_version(i)?;
    let (i, code) = parse_status_code(i)?;
    let (i, msg) = parse_status_text(i)?;

    let rstatus = HttpResponseStatus::new(version, code, msg);
    Ok((i, rstatus))
}

type ParseResponseRetType = (HttpResponseStatus, Vec<HttpHeader>, Vec<u8>, Vec<u8>);

fn nom_parse_response(i: &[u8]) -> IResult<&[u8], ParseResponseRetType, ()> {
    debug!("Entering nom_parse_response: {:?}", i);
    let copy = i;
    let (i, status_line) = parse_status_line(i)?;
    debug!("Status line parsed");
    let (body_au8, headers) = parse_headers(i)?;
    debug!("Headers parsed");
    let body: Vec<u8> = body_au8.to_vec();

    let head_len = copy.len() - body_au8.len();
    let raw = copy[..head_len].to_vec();

    let r = (status_line, headers, body, raw);

    Ok((body_au8, r))
}

fn handle_response_head<T>(reader: &mut BufReader<T>) -> Option<ParseResponseRetType>
where
    T: Read + Write,
{
    debug!("Entering handle_response_head");
    let r = reader.parse(nom_parse_response).ok()?;
    Some(r)
}

fn nom_take_one(i: &[u8]) -> IResult<&[u8], u8, ()> {
    let (i, o) = take(1u8)(i)?;
    Ok((i, o[0]))
}

fn read_until_size<T>(reader: &mut BufReader<T>, s: u64) -> Vec<u8>
where
    T: Read,
{
    let mut out: Vec<u8> = Vec::with_capacity(s as usize);

    let mut bytes_read = 0;

    while bytes_read < s {
        let b = reader.parse(nom_take_one).unwrap();
        out.push(b);
        bytes_read += 1;
    }
    debug!("read_until_size: {} [Wanted: {}]", bytes_read, s);
    out
}

fn handle_response<T, Y>(
    mut rstreamreader: BufReader<T>,
    mut s: Y,
) -> Result<HttpResponse, Box<dyn Error>>
where
    T: Read + Write,
    Y: Read + Write,
{
    debug!("Entering handle_response");
    let resp = handle_response_head(&mut rstreamreader);
    let resp = match resp {
        None => return Err("Could not parse response".into()),
        Some(x) => {
            debug!("Got response head");
            let (status, headers, _, head) = x;

            if s.write_all(&head).is_err() {
                return Err("Error while writing response head".into());
            }
            debug!("Written response head back to client");
            let mut r = HttpResponse::new(status, headers, vec![], head);

            if let Some(clen) = r.content_length() {
                debug!("Response had content-length: {}", clen);
                let body = r.body_mut();
                let mut data = read_until_size(&mut rstreamreader, (clen - body.len()) as u64);
                body.append(&mut data);
                debug!("Body before send: {:?}", &body);
                if let Err(e) = s.write_all(body) {
                    return Err(format!("Error while writing body to client: {}", e).into());
                }
            } else if r.transfer_encoding() == &HttpTransferEncoding::Chunked {
                debug!("Response is chunk encoded");
                let body = r.body_mut();
                loop {
                    /* get chunk */
                    let chunk =
                        read_chunk(&mut rstreamreader).ok_or("Error while reading chunk")?;
                    /* write chunk to response object */
                    body.extend_from_slice(&chunk.raw()[..]);
                    /* write chunk to client */
                    if let Err(e) = s.write_all(chunk.raw()) {
                        return Err(format!("Error while writing chunks: {}", e).into());
                    }
                    /* check chunk size to exit the loop */
                    if matches!(chunk.size(), 0) {
                        break;
                    }
                }
            }

            r
        }
    };

    Ok(resp)
}

fn handle_https_request(
    reader: BufReader<TcpStream>,
    req: HttpRequest,
    ca_cert: X509,
    ca_key_pair: PKey<Private>,
    config: Config,
) -> Result<(), Box<dyn Error>> {
    debug!("Entering handle_https_request");
    let now = Instant::now();
    let destination = req
        .destination()
        .ok_or("Could not get destination from request")?;
    debug!("Client wants to join {}", destination);
    let mut s = reader.into_inner();
    let remote_addr = format!("{}", s.peer_addr().unwrap());
    /*accept tunnel */
    s.write_all(
        "HTTP/1.1 200 OK\r\nProxy-Connection: Close\r\nConnection: Close\r\n\r\n".as_bytes(),
    )?;
    debug!("Accepted HTTPS tunneling");
    let mut host_no_port = req
        .destination_base()
        .ok_or("Error parsing host")?
        .to_owned();

    if host_no_port.contains(':') {
        host_no_port = host_no_port.split(':').take(1).collect();
    }

    let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
    let (cert, pair) = get_host_pair(&conn, &host_no_port, &ca_cert, &ca_key_pair);
    let mut acceptor = SslAcceptor::mozilla_intermediate(SslMethod::tls())?;
    acceptor.set_private_key(&pair)?;
    acceptor.set_certificate(&cert)?;
    acceptor.add_extra_chain_cert(ca_cert)?;
    acceptor.set_verify(SslVerifyMode::NONE);
    acceptor.check_private_key()?;
    let acceptor = acceptor.build();
    let ssl = Ssl::new(acceptor.context())?;
    let mut sslstream = SslStream::new(ssl, &s)?;

    Pin::new(&mut sslstream).accept()?;
    debug!("Standard TCPStream is now wrapped with SslStream and handshake is over");
    let mut reader = BufReader::with_capacity(1024 * 1024, sslstream);
    let req = handle_request(&mut reader);
    let req = match req {
        None => return Err("Error getting request from ssl upgraded socket".into()),
        Some(x) => {
            let (method, path, version, headers, body, raw) = x;
            HttpRequest::new(method, path, version, headers, body, raw, false)
        }
    };
    debug!("Received request from client via SSLStream");
    let rstream_std = if config.force_target().is_empty() {
        TcpStream::connect(destination)?
    } else {
        TcpStream::connect(config.force_target())?
    };
    let mut rstream = ssl_connect(&rstream_std, &host_no_port)
        .ok_or(format!("Error handshaking with {}", destination))?;
    rstream.write_all(&req.as_bytes())?;
    debug!("SSL handshake to destination is done");
    let rstreamreader = BufReader::with_capacity(1024 * 1024, rstream);
    let resp = handle_response(rstreamreader, reader.into_inner())?;
    info!(
        "{} {} [{}] - {:?}",
        req.method(),
        req.path(),
        resp.status(),
        now.elapsed()
    );
    insert_complete_request(&req, &resp, &remote_addr, true, &config, &now.elapsed());
    Ok(())
}

fn parse_chunk_header(i: &[u8]) -> IResult<&[u8], usize, ()> {
    let (i, header_au8) = take_until("\r\n")(i)?;

    let (_, size_s) = map_res(take(header_au8.len()), std::str::from_utf8)(header_au8)?;

    let size = usize::from_str_radix(size_s, 16).unwrap();

    let (i, _) = take(2u8)(i)?;

    Ok((i, size))
}

fn take_chunk_body(i: &[u8], size: usize) -> IResult<&[u8], Vec<u8>, ()> {
    let (i, chunkbody) = take(size)(i)?;
    let (i, _) = take(2u8)(i)?;

    let mut body = Vec::with_capacity(chunkbody.len());
    body.extend_from_slice(chunkbody);
    body.extend_from_slice(&[0x0d, 0x0a]);

    Ok((i, body))
}

fn nom_parse_chunk(i: &[u8]) -> IResult<&[u8], HttpChunk, ()> {
    let (rest, chunksize) = parse_chunk_header(i)?;
    let (rest, chunkbody) = take_chunk_body(rest, chunksize)?;

    let chunk_raw = &i[..i.len() - rest.len()];
    let chunk = HttpChunk::new(chunk_raw.to_vec(), chunkbody, chunksize);

    Ok((rest, chunk))
}

fn read_chunk<T>(r: &mut BufReader<T>) -> Option<HttpChunk>
where
    T: Read + Write,
{
    let chunk = r.parse(nom_parse_chunk).ok()?;

    Some(chunk)
}

fn handle_cleartext_request(
    reader: BufReader<TcpStream>,
    req: HttpRequest,
    config: Config,
) -> Result<(), Box<dyn Error>> {
    let now = Instant::now();
    let destination = req
        .destination()
        .ok_or("Couldn't get destination from request")?;
    let s = reader.into_inner();
    let remote_addr = format!("{}", s.peer_addr().unwrap());
    let mut rstream = if config.force_target().is_empty() {
        TcpStream::connect(destination)?
    } else {
        TcpStream::connect(config.force_target())?
    };
    let pattern = format!("http://{}/", req.destination_base().ok_or("")?);

    let mut bytes: Vec<u8>;
    if req.path()[..pattern.len()] == pattern {
        /* need to patch request */
        let status_line = format!(
            "{} /{} {}\r\n",
            req.method(),
            &req.path()[pattern.len()..],
            req.version(),
        );

        let default_status_line =
            format!("{} {} {}\r\n", req.method(), &req.path(), req.version(),);
        bytes = status_line.as_bytes().to_vec();
        bytes.extend_from_slice(&req.as_bytes()[default_status_line.len()..]);
    } else {
        bytes = req.as_bytes().to_vec();
    }
    debug!("REQ: {:?}", &bytes);

    rstream.write_all(&bytes)?;
    let rstreamreader = BufReader::with_capacity(1024 * 1024, rstream);

    let resp = handle_response(rstreamreader, s)?;
    info!(
        "{} {} [{}] - {:?}",
        req.method(),
        req.path(),
        resp.status(),
        now.elapsed()
    );
    insert_complete_request(&req, &resp, &remote_addr, false, &config, &now.elapsed());
    Ok(())
}

fn ssl_connect<'a>(
    rstream_std: &'a TcpStream,
    host_no_port: &str,
) -> Option<openssl::ssl::SslStream<&'a TcpStream>> {
    let mut err_count = 0;
    let mut connectorb = SslConnector::builder(SslMethod::tls()).unwrap();
    connectorb.set_verify(SslVerifyMode::NONE);
    let connector = connectorb.build();

    let rstream = loop {
        if err_count > 3 {
            error!(
                "Abort: Too many failures during SSL handshake: {}",
                &host_no_port
            );
            return None;
        }
        match connector.connect(host_no_port, rstream_std) {
            Ok(n) => break n,
            Err(e) => {
                err_count += 1;
                match e {
                    HandshakeError::WouldBlock(eb) => {
                        error!("Another WouldBlock: {:?}", eb);
                        continue;
                    }
                    HandshakeError::SetupFailure(err) => {
                        error!("Abort: SetupFailure mid SSL handshake: {:?}", err);
                        return None;
                    }
                    _ => {
                        continue;
                    }
                }
            }
        }
    };

    Some(rstream)
}

fn insert_complete_request(
    req: &HttpRequest,
    resp: &HttpResponse,
    remote_addr: &String,
    ssl: bool,
    config: &Config,
    response_time: &std::time::Duration,
) {
    let conn =
        Connection::open(format!("{}/hist.db", config.directory())).expect("Could not open DB");

    let uri = req.path();
    let method = format!("{}", req.method());
    let size = format!("{}", req.as_bytes().len());
    let raw = String::from_utf8_lossy(&req.as_bytes()).to_string();
    let ssl = match ssl {
        true => "1".to_string(),
        false => "0".to_string(),
    };
    let raw_resp = String::from_utf8_lossy(&resp.as_bytes()).to_string();
    let status = format!("{}", resp.status().code());
    let params = {
        match req.method() {
            HttpMethod::Post | HttpMethod::Put | HttpMethod::Patch => "1".to_string(),
            _ => {
                if req.path().contains('?') {
                    "1".to_string()
                } else {
                    "0".to_string()
                }
            }
        }
    };
    let raw_resp_time = format!("{:?}", response_time);
    conn.execute(
        "INSERT INTO history (uri, remote_addr, method, size, params, status, raw, ssl, response, response_time) VALUES (?1, ?10, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9)", 
        [uri, &method, &size, &params, &status, &raw, &ssl, &raw_resp, &raw_resp_time, remote_addr]
    ).unwrap();
}
