use core::pin::Pin;
use log::{debug, info};
use nom_bufreader::bufreader::BufReader;
use nomhttp::http_method::HttpMethod;
use nomhttp::http_request::HttpRequest;
use openssl::{
    pkey::{PKey, Private},
    ssl::{Ssl, SslAcceptor, SslMethod, SslStream, SslVerifyMode},
    x509::X509,
};
use rusqlite::Connection;
use std::error::Error;
use std::io::Write;
use std::net::{TcpListener, TcpStream};
use std::thread;

use crate::api_response::ApiResponse;
use crate::client_manager::handle_request;
use crate::config::Config;
use crate::history::HistLine;
use crate::inspector::Inspector;

macro_rules! wrap {
    ($s: expr, $cert: expr, $pair: expr, $ca_cert: expr) => {{
        let mut acceptor = SslAcceptor::mozilla_intermediate(SslMethod::tls())?;
        acceptor.set_private_key($pair)?;
        acceptor.set_certificate($cert)?;
        acceptor.add_extra_chain_cert($ca_cert)?;
        acceptor.set_verify(SslVerifyMode::NONE);
        acceptor.check_private_key()?;
        let acceptor = acceptor.build();
        let ssl = Ssl::new(acceptor.context())?;
        let sslstream = SslStream::new(ssl, &$s)?;

        sslstream
    }};
}

macro_rules! is_auth {
    ($req: expr, $shared_secret: expr) => {{
        let mut res = false;
        for h in $req.headers() {
            if (h.name().as_str() == "Authentication" || h.name().as_str() == "authentication")
                && h.value() == &format!("Bearer {}", $shared_secret)
            {
                res = true;
                break;
            }
        }
        res
    }};
}

fn _handle_api(
    cert: X509,
    pair: PKey<Private>,
    ca_cert: X509,
    config: Config,
) -> Result<(), Box<dyn Error>> {
    let addr = format!("{}:{}", config.api_addr(), config.api_port());
    let listener = TcpListener::bind(&addr)?;
    info!("Api listening on: {}", addr);

    for stream in listener.incoming() {
        let s = stream?;
        let cert = cert.clone();
        let pair = pair.clone();
        let ca_cert = ca_cert.clone();
        let cfg = config.clone();

        thread::spawn(move || handle_client(s, &cert, &pair, ca_cert, cfg));
    }

    Ok(())
}

pub fn handle_api(
    cert: X509,
    pair: PKey<Private>,
    ca_cert: X509,
    config: Config,
) -> bool {
    _handle_api(cert, pair, ca_cert, config).is_ok()
}

fn _handle_client(
    s: TcpStream,
    cert: &X509,
    pair: &PKey<Private>,
    ca_cert: X509,
    config: Config,
) -> Result<(), Box<dyn Error>> {
    let cacert = ca_cert.clone();
    let mut sslstream = wrap!(s, cert, pair, ca_cert);
    Pin::new(&mut sslstream).accept()?;
    let mut reader = BufReader::with_capacity(1024 * 1024, sslstream);
    let (method, path, version, headers, body, raw) =
        handle_request(&mut reader).ok_or("Error while parsing request")?;

    let req = HttpRequest::new(method, path, version, headers, body, raw, false);
    let resp = match req.path().chars().take(5).collect::<String>().as_str() {
        "/" => ApiResponse::default().with_html("<html><body>Hello world!</body></html>"),
        "/cert" | "/cacert" | "/cacert.der" | "/cacert.pem" | "/cert.der" | "/cert.pem" => handle_cert_route(&cacert),
        "/api/" => handle_api_route(&req, config)?,
        _ => ApiResponse::default().with_status(404),
    };

    let mut stream = reader.into_inner();
    stream.write_all(&resp.as_bytes())?;

    stream.shutdown()?;
    debug!(
        "[API] {} {} [{}]",
        req.method(),
        req.path(),
        resp.status().as_str(),
    );
    Ok(())
}

fn handle_client(
    s: TcpStream,
    cert: &X509,
    pair: &PKey<Private>,
    ca_cert: X509,
    config: Config,
) -> bool {
    _handle_client(s, cert, pair, ca_cert, config).is_ok()
}


fn handle_cert_route(ca_cert: &X509) -> ApiResponse {
    let pem = ca_cert.to_pem().unwrap();
    let mut resp = ApiResponse::default().with_status(200).with_body(String::from_utf8_lossy(&pem).to_string()).with_content_type("application/x-x509-
    ca-cert");
    let headers = resp.headers_mut();
    headers.with_header("Content-Disposition".to_string(), "inline; filename=\"cacert.der\"".to_string());
    resp
}


fn handle_api_route(r: &HttpRequest, config: Config) -> Result<ApiResponse, Box<dyn Error>> {
    let mut resp = if is_auth!(r, config.shared_secret()) {
        let parts = r
            .path()
            .split("/api/")
            .skip(1)
            .take(1)
            .map(|s| s.to_string())
            .collect::<String>()
            .split('/')
            .map(|s| s.to_string())
            .collect::<Vec<String>>();
        let resp = match parts[0].as_str() {
            "inspectors" => handle_inspector_api(r, parts, &config),
            "requests" => handle_requests_api(r, parts, &config),
            _ => ApiResponse::default().with_status(404),
        };
        resp
    } else {
        ApiResponse::default().with_status(403)
    };

    resp.headers_mut()
        .with_header("Access-Control-Allow-Origin".to_string(), "*".to_string());

    Ok(resp)
}

fn handle_inspector_api(r: &HttpRequest, parts: Vec<String>, config: &Config) -> ApiResponse {
    match _handle_inspector_api(r, parts, config) {
        Ok(r) => r,
        Err(e) => ApiResponse::default().with_status(500).with_json(format!(
            "{{\"error\": {}}}",
            json::stringify(format!("{:?}", e))
        )),
    }
}

fn _handle_inspector_api(
    r: &HttpRequest,
    parts: Vec<String>,
    config: &Config,
) -> Result<ApiResponse, Box<dyn Error>> {
    match r.method() {
        HttpMethod::Get => list_or_get_inspectors(config, &parts),
        HttpMethod::Patch => update_inspector(r, config, &parts),
        HttpMethod::Post => new_inspector(r, config),
        HttpMethod::Delete => del_inspector(config, &parts),
        _ => Ok(ApiResponse::default().with_status(405)),
    }
}

fn list_or_get_inspectors(
    config: &Config,
    parts: &[String],
) -> Result<ApiResponse, Box<dyn Error>> {
    let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
    let rid = parts.iter().skip(1).take(1).next();
    if rid.is_some() && !rid.unwrap().is_empty() {
        let id: usize = rid.unwrap().parse()?;
        let mut stmt = conn.prepare("SELECT * FROM inspectors WHERE id=?")?;
        let rinspector = stmt.query_row([id], |row| {
            let json: String = row.get(7)?;
            let bf_results = match serde_json::from_str(json.as_str()) {
                Ok(s) => Ok::<Vec<String>, rusqlite::Error>(s),
                Err(_) => {
                    Ok(vec![json])
                }
            }?;
            let ssl: usize = row.get(5)?;

            let i = Inspector {
                id: row.get(0)?,
                request: row.get(1)?,
                response: row.get(2)?,
                modified_request: row.get(3)?,
                new_response: row.get(4)?,
                ssl: ssl == 1,
                target: row.get(6)?,
                bf_results,
                bf_request: row.get(8)?,
            };
            Ok(i)
        });
        if let Err(e) = rinspector {
            match e {
                rusqlite::Error::QueryReturnedNoRows => Ok(ApiResponse::default().with_status(404).with_json("{\"error\": \"bad ID\"}")),
                _ => Err(Box::new(e)),
            }
        } else {
            let inspector = rinspector.unwrap();
            let out = serde_json::to_string(&inspector)?;
            Ok(ApiResponse::default().with_json(out))
        }
    } else {
        let mut stmt = conn.prepare("SELECT * FROM inspectors")?;
        let inspectors = stmt.query_map([], |row| {
            let json: String = row.get(7)?;
            let bf_results = match serde_json::from_str(json.as_str()) {
                Ok(s) => Ok::<Vec<String>, rusqlite::Error>(s),
                Err(e) => Ok(vec![json, e.to_string()]),
            }?;
            let ssl: usize = row.get(5)?;

            let i = Inspector {
                id: row.get(0)?,
                request: row.get(1)?,
                response: row.get(2)?,
                modified_request: row.get(3)?,
                new_response: row.get(4)?,
                ssl: ssl == 1,
                target: row.get(6)?,
                bf_results,
                bf_request: row.get(8)?,
            };
            Ok(i)
        })?;
        let mut out = vec![];
        for inspector in inspectors {
            out.push(inspector?);
        }
        let out = serde_json::to_string(&out)?;
        Ok(ApiResponse::default().with_json(out))
    }
}

fn update_inspector(
    r: &HttpRequest,
    config: &Config,
    parts: &[String],
) -> Result<ApiResponse, Box<dyn Error>> {
    let rid = parts
        .iter()
        .skip(1)
        .take(1)
        .next();
    if rid != Some(&"".to_string()) {
        let id = rid.unwrap();
        let inspector: Inspector =
        serde_json::from_str(&r.body().iter().map(|&x| x as char).collect::<String>())?;
        let ssl = if inspector.ssl {
            "1".to_string()
        } else {
            "0".to_string()
        };
        let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
        let mut stmt = conn.prepare("UPDATE inspectors SET request=?, response=?, modified_request=?, new_response=?, ssl=?, target=?, bf_results=?, bf_request=? WHERE id=? RETURNING id")?;
        let ri = stmt.query_row(
            [
                inspector.request,
                inspector.response,
                inspector.modified_request,
                inspector.new_response,
                ssl,
                inspector.target,
                serde_json::to_string(&inspector.bf_results)?,
                inspector.bf_request,
                id.to_string(),
            ],
            |row| {
                let i: usize = row.get(0)?;
                Ok(i)
            },
        );
        match ri {
            Ok(i) => Ok(ApiResponse::default().with_json(format!("{{\"id\": {}}}", json::stringify(i)))),
            Err(e) => match e {
                rusqlite::Error::QueryReturnedNoRows => Ok(ApiResponse::default().with_status(404).with_json("{\"error\": \"bad ID\"}")),
                _ => Err(Box::new(e)),
            }
        }
        
    } else {
        Ok(ApiResponse::default().with_status(404).with_json("{\"error\": \"Missing ID\"}"))
    }
    
}

fn new_inspector(r: &HttpRequest, config: &Config) -> Result<ApiResponse, Box<dyn Error>> {
    let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
    let mut stmt = conn.prepare("INSERT INTO inspectors(request, response, modified_request, new_response, ssl, target, bf_results, bf_request) VALUES(?, ?, ?, ?, ?, ?, ?, ?) RETURNING id")?;
    let inspector: Inspector = match serde_json::from_str(&r.body().iter().map(|&x| x as char).collect::<String>()){
        Ok(i) => i,
        Err(_) => return Ok(ApiResponse::default().with_status(400).with_json("{\"error\":\"Invalid or missing JSON\"}")),
    };
    let ssl = if inspector.ssl {
        "1".to_string()
    } else {
        "0".to_string()
    };
    let i = stmt.query_row(
        [
            inspector.request,
            inspector.response,
            inspector.modified_request,
            inspector.new_response,
            ssl,
            inspector.target,
            serde_json::to_string(&inspector.bf_results)?,
            inspector.bf_request,
        ],
        |row| {
            let i: usize = row.get(0)?;
            Ok(i)
        },
    )?;
    Ok(ApiResponse::default().with_json(format!("{{\"id\": {}}}", json::stringify(i))))
}

fn del_inspector(config: &Config, parts: &[String]) -> Result<ApiResponse, Box<dyn Error>> {
    let rid = parts
        .iter()
        .skip(1)
        .take(1)
        .next();
    if rid != Some(&"".to_string()) {
        let id = rid.unwrap();
        let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
        let mut stmt = conn.prepare("DELETE FROM inspectors WHERE id=? RETURNING id")?;
        let ri = stmt.query_row([id], |row| {
            let i: usize = row.get(0)?;
            Ok(i)
        });
        match ri {
            Ok(i) => Ok(ApiResponse::default().with_json(format!("{{\"id\": {}}}", json::stringify(i)))),
            Err(e) => match e {
                rusqlite::Error::QueryReturnedNoRows => Ok(ApiResponse::default().with_status(404).with_json("{\"error\": \"bad ID\"}")),
                _ => Err(Box::new(e)),
            }
        }
        
    } else {
        Ok(ApiResponse::default().with_status(404).with_json("{\"error\": \"Missing ID\"}"))
    }
    
}

fn handle_requests_api(r: &HttpRequest, parts: Vec<String>, config: &Config) -> ApiResponse {
    match _handle_requests_api(r, parts, config) {
        Ok(r) => r,
        Err(e) => ApiResponse::default().with_status(500).with_json(format!(
            "{{\"error\": {}}}",
            json::stringify(format!("{:?}", e))
        )),
    }
}

fn _handle_requests_api(
    r: &HttpRequest,
    parts: Vec<String>,
    config: &Config,
) -> Result<ApiResponse, Box<dyn Error>> {
    match r.method() {
        HttpMethod::Get => list_or_get_requests(config, &parts),
        _ => Ok(ApiResponse::default().with_status(405)),
    }
}

fn list_or_get_requests(config: &Config, parts: &[String]) -> Result<ApiResponse, Box<dyn Error>> {
    let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
    let last_id = parts.iter().skip(1).take(1).next();
    if last_id.is_some() && !last_id.unwrap().is_empty() {
        let id: usize = last_id.unwrap().parse()?;
        let mut stmt = conn.prepare("SELECT * FROM history WHERE id > ? ORDER BY id Asc")?;
        let rows = stmt.query_map([id], |row| {
            Ok(HistLine {
                id: row.get(0).unwrap(),
                remote_addr: row.get(1).unwrap(),
                uri: row.get(2).unwrap(),
                method: row.get(3).unwrap(),
                size: row.get(6).unwrap(),
                params: matches!(row.get(4).unwrap(), 1),
                status: row.get(5).unwrap(),
                raw: row.get(7).unwrap(),
                ssl: row.get(8).unwrap(),
                response: row.get(9).unwrap(),
                response_time: row.get(10).unwrap(),
                host: row
                    .get::<usize, String>(7)
                    .unwrap()
                    .split("ost: ")
                    .skip(1)
                    .take(1)
                    .collect::<String>()
                    .split("\r\n")
                    .take(1)
                    .collect::<String>(),
            })
        })?;
        let mut out = vec![];
        for histline in rows {
            out.push(histline?);
        }
        let out = serde_json::to_string(&out)?;
        Ok(ApiResponse::default().with_json(out))
    } else {
        Ok(ApiResponse::default().with_status(404).with_json("{\"Missing last_id\": \"Missing last id\"}"))
    }
}


#[cfg(test)]
mod tests {
    use crate::api_response::ApiResponseStatus;
    use crate::api_manager::handle_api_route;
    use crate::inspector::Inspector;
    use nomhttp::http_request::HttpRequest;
    use nomhttp::http_method::HttpMethod;
    use nomhttp::http_version::HttpVersion;
    use nomhttp::http_header::HttpHeader;
    use rusqlite::Connection;
    use crate::config::Config;
    use std::sync::Once;
    use std::path::Path;
    use std::fs;

    static INIT: Once = Once::new();

    fn initialize() {
        INIT.call_once(|| {
            match Path::new("./test_project/").exists() {
                true => {}
                false => {
                    fs::create_dir("./test_project/").unwrap();
                }
            }
            let conn = Connection::open("./test_project/hist.db").unwrap();
            conn.execute("DROP TABLE IF EXISTS history", []).unwrap();
            conn.execute(
                "CREATE TABLE history(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                remote_addr TEXT,
                uri TEXT,
                method TEXT,
                params INTEGER,
                status INTEGER,
                size INTEGER,
                raw BLOB,
                ssl INTEGER,
                response TEXT,
                response_time TEXT)",
                [],
            ).unwrap();

            conn.execute("DROP TABLE IF EXISTS inspectors", []).unwrap();
            conn.execute(
                "CREATE TABLE inspectors(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                request BLOB,
                response BLOB,
                modified_request BLOB,
                new_response BLOB,
                ssl INTEGER,
                target TEXT,
                bf_results BLOB,
                bf_request BLOB)",
                [],
            ).unwrap();

            conn.execute(
                "INSERT INTO inspectors(request, response, modified_request, new_response, ssl, target, bf_results, bf_request) VALUES(
                    \"\", \"\", \"\", \"\", 0, \"\", \"\", \"\"
                )",
                [],
            ).unwrap();
        });
    }


    #[test]
    fn get_api_bad_auth_generates_403() {
        let r = HttpRequest::new(
            HttpMethod::Get,
            "/api/test".to_string(),
            HttpVersion::Http11,
            vec![],
            vec![],
            vec![],
            true
        );

        let cfg = Config::default();
        if let Ok(res) = handle_api_route(&r, cfg) {
            assert_eq!(res.status(), &ApiResponseStatus::Forbidden);
        }
    }

    #[test]
    fn get_api_random_good_auth_generates_404() {
        let auth = HttpHeader::new("Authentication".to_string(), "Bearer tests".to_string());
        let r = HttpRequest::new(
            HttpMethod::Get,
            "/api/test".to_string(),
            HttpVersion::Http11,
            vec![auth],
            vec![],
            vec![],
            true
        );
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");

        if let Ok(res) = handle_api_route(&r, cfg) {
            assert_eq!(res.status(), &ApiResponseStatus::NotFound);
        }
    }

    #[test]
    fn get_api_requests_good_auth_good_id_generates_200() {
        initialize();

        let auth = HttpHeader::new("Authentication".to_string(), "Bearer tests".to_string());
        let r = HttpRequest::new(
            HttpMethod::Get,
            "/api/requests/0".to_string(),
            HttpVersion::Http11,
            vec![auth],
            vec![],
            vec![],
            true
        );
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            assert_eq!(res.status(), &ApiResponseStatus::Ok);
        }
    }

    #[test]
    fn get_api_requests_good_auth_bad_id_generates_404() {
        initialize();

        let auth = HttpHeader::new("Authentication".to_string(), "Bearer tests".to_string());
        let r = HttpRequest::new(
            HttpMethod::Get,
            "/api/requests/123456".to_string(),
            HttpVersion::Http11,
            vec![auth],
            vec![],
            vec![],
            true
        );
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            assert_eq!(res.status(), &ApiResponseStatus::Ok);
        }
    }

    #[test]
    fn get_api_requests_bad_auth_generates_403() {

        let r = HttpRequest::new(
            HttpMethod::Get,
            "/api/requests/0".to_string(),
            HttpVersion::Http11,
            vec![],
            vec![],
            vec![],
            true
        );
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            assert_eq!(res.status(), &ApiResponseStatus::Forbidden);
        }
    }

    #[test]
    fn get_api_inspectors_bad_auth_generates_403() {

        let r = HttpRequest::new(
            HttpMethod::Get,
            "/api/inspectors".to_string(),
            HttpVersion::Http11,
            vec![],
            vec![],
            vec![],
            true
        );
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            assert_eq!(res.status(), &ApiResponseStatus::Forbidden);
        }
    }

    #[test]
    fn get_api_inspectors_good_auth_generates_200() {
        initialize();

        let auth = HttpHeader::new("Authentication".to_string(), "Bearer tests".to_string());
        let r = HttpRequest::new(
            HttpMethod::Get,
            "/api/inspectors".to_string(),
            HttpVersion::Http11,
            vec![auth],
            vec![],
            vec![],
            true
        );
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            assert_eq!(res.status(), &ApiResponseStatus::Ok);
        }
    }

    #[test]
    fn get_api_inspectors_good_auth_and_bad_id_generates_404() {
        initialize();

        let auth = HttpHeader::new("Authentication".to_string(), "Bearer tests".to_string());
        let r = HttpRequest::new(
            HttpMethod::Get,
            "/api/inspectors/123456".to_string(),
            HttpVersion::Http11,
            vec![auth],
            vec![],
            vec![],
            true
        );
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            assert_eq!(res.status(), &ApiResponseStatus::NotFound);
        }
    }

    #[test]
    fn del_api_inspectors_good_auth_no_id_generates_404() {
        initialize();

        let auth = HttpHeader::new("Authentication".to_string(), "Bearer tests".to_string());
        let r = HttpRequest::new(
            HttpMethod::Delete,
            "/api/inspectors/".to_string(),
            HttpVersion::Http11,
            vec![auth],
            vec![],
            vec![],
            true
        );
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            assert_eq!(res.status(), &ApiResponseStatus::NotFound);
        }
    }

    #[test]
    fn del_api_inspectors_good_auth_bad_id_generates_404() {
        initialize();

        let auth = HttpHeader::new("Authentication".to_string(), "Bearer tests".to_string());
        let r = HttpRequest::new(
            HttpMethod::Delete,
            "/api/inspectors/123456".to_string(),
            HttpVersion::Http11,
            vec![auth],
            vec![],
            vec![],
            true
        );
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            assert_eq!(res.status(), &ApiResponseStatus::NotFound);
        }
    }

    #[test]
    fn del_api_inspectors_good_auth_good_id_generates_200() {
        initialize();

        let auth = HttpHeader::new("Authentication".to_string(), "Bearer tests".to_string());
        let r = HttpRequest::new(
            HttpMethod::Delete,
            "/api/inspectors/1".to_string(),
            HttpVersion::Http11,
            vec![auth],
            vec![],
            vec![],
            true
        );
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            assert_eq!(res.status(), &ApiResponseStatus::Ok);
        }
    }

    #[test]
    fn new_api_inspectors_good_auth_good_json_generates_200() {
        initialize();

        let auth = HttpHeader::new("Authentication".to_string(), "Bearer tests".to_string());
        let json = serde_json::to_string(&Inspector::default()).unwrap();

        let r = HttpRequest::new(
            HttpMethod::Post,
            "/api/inspectors/".to_string(),
            HttpVersion::Http11,
            vec![auth],
            json.as_bytes().to_vec(),
            vec![],
            true
        );
        
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            println!("{:?}", res);
            assert_eq!(res.status(), &ApiResponseStatus::Ok);
        }
    }

    #[test]
    fn new_api_inspectors_good_auth_bad_json_generates_400() {
        initialize();

        let auth = HttpHeader::new("Authentication".to_string(), "Bearer tests".to_string());

        let r = HttpRequest::new(
            HttpMethod::Post,
            "/api/inspectors/".to_string(),
            HttpVersion::Http11,
            vec![auth],
            "".as_bytes().to_vec(),
            vec![],
            true
        );
        
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            println!("{:?}", res);
            assert_eq!(res.status(), &ApiResponseStatus::BadRequest);
        }
    }
    
    #[test]
    fn update_api_inspectors_good_auth_good_id_generates_200() {
        initialize();

        let auth = HttpHeader::new("Authentication".to_string(), "Bearer tests".to_string());
        let mut inspector = Inspector::default();
        inspector.target="httpbin.org".to_string();
        let json = serde_json::to_string(&inspector).unwrap();
        let r = HttpRequest::new(
            HttpMethod::Patch,
            "/api/inspectors/2".to_string(), // id will be 2 after the "new" above
            HttpVersion::Http11,
            vec![auth],
            json.as_bytes().to_vec(),
            vec![],
            true
        );
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            println!("{:?}", res);
            assert_eq!(res.status(), &ApiResponseStatus::Ok);
        }
    }

    #[test]
    fn update_api_inspectors_good_auth_bad_id_generates_400() {
        initialize();

        let auth = HttpHeader::new("Authentication".to_string(), "Bearer tests".to_string());
        let mut inspector = Inspector::default();
        inspector.target="httpbin.org".to_string();
        let json = serde_json::to_string(&inspector).unwrap();
        let r = HttpRequest::new(
            HttpMethod::Patch,
            "/api/inspectors/0".to_string(), // id will be 2 after the "new" above
            HttpVersion::Http11,
            vec![auth],
            json.as_bytes().to_vec(),
            vec![],
            true
        );
        let mut cfg = Config::default();
        cfg.set_shared_secret("tests");
        cfg.set_directory("./test_project/");

        if let Ok(res) = handle_api_route(&r, cfg) {
            println!("{:?}", res);
            assert_eq!(res.status(), &ApiResponseStatus::NotFound);
        }
    }
}