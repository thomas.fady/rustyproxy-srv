use openssl::{
    pkey::{PKey, PKeyRef, Private},
    x509::{X509Ref, X509},
};
use rusqlite::Connection;

use crate::ssl::{mk_ca_cert, mk_ca_signed_cert};

pub fn get_ca_pair(conn: &Connection) -> (X509, PKey<Private>) {
    let mut stmt = conn
        .prepare("SELECT cert, private FROM ca WHERE id=1")
        .unwrap();
    let result: Result<(Result<String, _>, Result<String, _>), _> =
        stmt.query_row([], |r| Ok((r.get(0), r.get(1))));
    let (ca_cert, ca_key_pair) = match result {
        Err(_) => {
            let (cert, key) = mk_ca_cert().expect("Could not generate CA");

            let mut stmt = conn
                .prepare("INSERT INTO ca(cert, private, hostname) VALUES(?1, ?2, ?3)")
                .unwrap();
            let cert_pem = String::from_utf8_lossy(&cert.to_pem().unwrap()).to_string();
            let key_pem =
                String::from_utf8_lossy(&key.private_key_to_pem_pkcs8().unwrap()).to_string();
            stmt.execute([&cert_pem, &key_pem, "CA"]).unwrap();

            (cert, key)
        }
        Ok((cr, kr)) => {
            let cert_pem = cr.unwrap();
            let key_pem = kr.unwrap();

            let x509 = X509::from_pem(cert_pem.as_bytes()).unwrap();
            let private = PKey::private_key_from_pem(key_pem.as_bytes()).unwrap();

            (x509, private)
        }
    };

    (ca_cert, ca_key_pair)
}

pub fn get_host_pair(
    conn: &Connection,
    hostname: &str,
    ca_cert: &X509Ref,
    ca_key_pair: &PKeyRef<Private>,
) -> (X509, PKey<Private>) {
    let mut stmt = conn
        .prepare("SELECT cert, private FROM ca WHERE hostname=?1")
        .unwrap();
    let result: Result<(Result<String, _>, Result<String, _>), _> =
        stmt.query_row([hostname], |r| Ok((r.get(0), r.get(1))));

    let (cert, pair) = match result {
        Err(_) => {
            let (cert, key) = mk_ca_signed_cert(ca_cert, ca_key_pair, hostname).unwrap();

            let mut stmt = conn
                .prepare("INSERT INTO ca(cert, private, hostname) VALUES(?1, ?2, ?3)")
                .unwrap();
            let cert_pem = String::from_utf8_lossy(&cert.to_pem().unwrap()).to_string();
            let key_pem =
                String::from_utf8_lossy(&key.private_key_to_pem_pkcs8().unwrap()).to_string();
            stmt.execute([&cert_pem, &key_pem, hostname]).unwrap();

            (cert, key)
        }
        Ok((cr, kr)) => {
            let cert_pem = cr.unwrap();
            let key_pem = kr.unwrap();

            let x509 = X509::from_pem(cert_pem.as_bytes()).unwrap();
            let private = PKey::private_key_from_pem(key_pem.as_bytes()).unwrap();

            (x509, private)
        }
    };

    (cert, pair)
}
