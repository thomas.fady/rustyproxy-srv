#[derive(serde::Deserialize, serde::Serialize, Debug)]
pub struct HistLine {
    pub id: usize,
    pub remote_addr: String,
    pub uri: String,
    pub method: String,
    pub params: bool,
    pub status: usize,
    pub size: usize,
    pub raw: String,
    pub ssl: bool,
    pub response: String,
    pub response_time: String,
    pub host: String,
}
