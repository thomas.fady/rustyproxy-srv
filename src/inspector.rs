#[derive(Default, Debug, serde::Serialize, serde::Deserialize)]
pub struct Inspector {
    pub id: usize,
    pub request: String,
    pub response: String,
    pub modified_request: String,
    pub new_response: String,
    pub ssl: bool,
    pub target: String,
    pub bf_results: Vec<String>,
    pub bf_request: String,
}
