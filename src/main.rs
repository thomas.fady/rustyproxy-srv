use chrono::Local;
use clap::Parser;
use env_logger::Builder;
use log::{info, LevelFilter};
use rusqlite::Connection;
use std::io::Write;
use std::net::TcpListener;
use std::thread;
use std::{error::Error, fs, path::Path};

mod api_manager;
mod api_response;
mod client_manager;
mod config;
mod history;
mod inspector;
mod ssl;
mod ssl_manager;

use crate::api_manager::handle_api;
use crate::client_manager::client_manager;
use crate::config::Config;
use crate::ssl_manager::{get_ca_pair, get_host_pair};

fn main() -> Result<(), Box<dyn Error>> {
    let config = Config::parse();

    let log_filter = match config.debug() {
        true => LevelFilter::Debug,
        false => LevelFilter::Info,
    };

    Builder::new()
        .format(|buf, record| {
            writeln!(
                buf,
                "{} [{}] - {}",
                Local::now().format("%Y-%m-%dT%H:%M:%S"),
                record.level(),
                record.args()
            )
        })
        .filter(None, log_filter)
        .init();

    match Path::new(config.directory()).exists() {
        true => {}
        false => {
            fs::create_dir(config.directory())?;
        }
    }

    let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS history(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        remote_addr TEXT,
        uri TEXT,
        method TEXT,
        params INTEGER,
        status INTEGER,
        size INTEGER,
        raw BLOB,
        ssl INTEGER,
        response TEXT,
        response_time TEXT)",
        [],
    )?;

    conn.execute(
        "CREATE TABLE IF NOT EXISTS ca(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        hostname TEXT,
        cert TEXT,
        private TEXT)",
        [],
    )?;

    conn.execute(
        "CREATE TABLE IF NOT EXISTS inspectors(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        request BLOB,
        response BLOB,
        modified_request BLOB,
        new_response BLOB,
        ssl INTEGER,
        target TEXT,
        bf_results BLOB,
        bf_request BLOB)",
        [],
    )?;

    let addr = config.full_addr();

    let listener = TcpListener::bind(&addr)?;
    info!("Socket bound on: {}", addr);

    let (ca_cert, ca_key_pair) = get_ca_pair(&conn);
    let apiaddr = format!("{}:{}", config.api_addr(), config.api_port());
    let (cert, pair) = get_host_pair(&conn, &apiaddr, &ca_cert, &ca_key_pair);
    let ncacert = ca_cert.clone();
    let ncfg = config.clone();
    thread::spawn(move || handle_api(cert, pair, ncacert, ncfg));
    for stream in listener.incoming() {
        let s = stream?;
        let ca_cert = ca_cert.clone();
        let ca_key_pair = ca_key_pair.clone();
        let cfg = config.clone();
        thread::spawn(move || client_manager(s, ca_cert, ca_key_pair, cfg));
    }
    Ok(())
}
