# rustyproxy-srv

This is server for [the rustyproxy project](https://gitlab.com/r2367/).
If you search for the GUI see [this repository](https://github.com/Vaelio/rustyproxy-egui)

## Contributing

At this point the priority is making a user interface to be able to view/edit/replay requests. The overall goal is to provide the equivalent of the repeater and intruder of Burpsuite. But this part of the project should only store request coming from browsers. 

TL;DR: Message me on Matrix and i'll guide you: [@vaelio](https://matrix.to/#/@vaelio:matarch.fr)

## Getting started

If you are using the binary:

```sh
$ rustyproxy -h

rustyproxy
MITM Proxy server that stores requests in sqlite for later usage

USAGE:
    rustyproxy-srv [OPTIONS] --directory <DIRECTORY> --secret <SECRET>

OPTIONS:
    -a, --addr <ADDR>                    addr to bind to [default: 127.0.0.1]
    -A, --api-addr <API_ADDR>            addr for the api [default: 127.0.0.1]
        --allow-localhost                disable localhost protection
    -d, --directory <DIRECTORY>          Path for the project to be stored
        --force-target <FORCE_TARGET>    force target for any incoming request [default: ]
    -h, --help                           Print help information
    -p, --port <PORT>                    port to bind to [default: 8000]
    -P, --api-port <API_PORT>            port for the api [default: 8443]
    -s, --secret <SECRET>                shared secret for the API
    -v, --verbose                        allow debug logs
```

if you are using cargo:

```sh
$ cargo run -- [OPTIONS] --directory <DIRECTORY> -s <SECRET>
```

For example:

```sh
$ cargo run --release -- -a 0.0.0.0 -p 8080 -d /tmp/rustyproxy-project/ -s SUPAHSECRET
```

## Roadmap

This part of the project is now in a somewhat usable state. 
There are still bugs that I need to trace but it should be mostly fine.
As a work around if you have any high CPU usage just restart the proxy and it should be fine.
If you ever have the time to trace which requests created the problem feel free to send it to me so that I can reproduce and fix.

- [x] HTTPS Mitm proxy
- [x] HTTP proxy
- [x] HTTP(s) Reverse Proxy
- [x] HTTPS Api for GUIs 
- [ ] Upstream Proxy
- [ ] Handle websockets
- [ ] Code optimization and bug termination


## What kind of performance should i expect ?

For now it should be "decent" but i'm sure someone more experienced should be able to optimize further.

However, as an exemple, i get pretty consistent results like this one:
(Most likely, the performance advantage with the proxy comes from the fact that i ignore certificates validation)

```
❯ time curl https://httpbin.org/ip 1>/dev/null 2>&1

real	0m0.405s
user	0m0.027s
sys	0m0.025s

~
❯ time curl https://httpbin.org/ip -k --proxy-insecure --proxy 127.0.0.1:8000 1>/dev/null 2>&1

real	0m0.368s
user	0m0.004s
sys	0m0.018s
```

This program is licensed under GPL-3.0
